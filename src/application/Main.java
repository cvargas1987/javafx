package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@FXML
	static Label msn = new Label(); 
	
	@Override
	public void start(Stage Stage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("principal.fxml")); 
		
		// prueba
		Stage.setScene(new Scene(root));
		Stage.setResizable(true);
		Stage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
		msn.setText("Hola");
	}
}
